package com.sec.frecog;

import java.io.*; 
import java.awt.event.*; 
import com.googlecode.javacv.*;

import com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;
import static com.googlecode.javacv.cpp.opencv_objdetect.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import static com.googlecode.javacv.cpp.opencv_calib3d.*;
 
public class FaceDetection{
 	
	public static final String XML_FILE = "/home/mridup/Desktop/javacv-exp/frecog/src/main/resources/haarcascade_frontalface_default.xml";
	private static boolean isRunning = true;
	public static CanvasFrame canvas = null;	
	public static boolean canvasCreated = false;	
	public static OpenCVFaceRecognizer faceRecognizer;// = new OpenCVFaceRecognizer();
	public static int result, fileNos=0, positive=0;
	//public static char text[16];
	
	FaceDetection(OpenCVFaceRecognizer frecog) {
		faceRecognizer = frecog;
	}
	
	public static void main(String[] args){	
		
		
	}	
	 
	public static IplImage detect(IplImage src, int no){		
	
		CvHaarClassifierCascade cascade = new CvHaarClassifierCascade(cvLoad(XML_FILE));
		CvMemStorage storage = CvMemStorage.create();
		CvSeq sign = cvHaarDetectObjects(src, cascade, storage, 1.5, 3, CV_HAAR_DO_CANNY_PRUNING);
		CvFont font = new CvFont(CV_FONT_HERSHEY_PLAIN, 2.5, 2); 

		if (!canvasCreated)
		{
			canvas = new CanvasFrame("Result");       
			canvas.setDefaultCloseOperation(CanvasFrame.EXIT_ON_CLOSE);
			canvas.addWindowListener(new SetBoolean());
			canvasCreated = true;
		}
		 
		cvClearMemStorage(storage);
		 
		int total_Faces = sign.total();	
		
		System.out.println("Found " + total_Faces + " face(s)"); 

		for(int i = 0; i < total_Faces; i++) {
		
		CvRect r = new CvRect(cvGetSeqElem(sign, i));
		cvSetImageROI(src, r);//Set the ROI
		//Create a new image
		IplImage crop = cvCreateImage(cvSize(92,112),IPL_DEPTH_8U, 3 );
		
		//copy the new image
		//cvCopy(src,crop);
		

		//use cvResize to resize source to a destination image
		cvResize(src, crop, CV_INTER_LINEAR);

		cvSaveImage("/home/mridup/Desktop/javacv-exp/frecog/training/video/grab_resize/"+no+"_"+i+"_ncr.jpg", crop);
		
		//We can do the recognition part here and also display the name of the person on the image?
		result = faceRecognizer.run("/home/mridup/Desktop/javacv-exp/frecog/training/video/grab_resize/"+no+"_"+i+"_ncr.jpg");
		if (result==2) positive++;
		fileNos++;
		
		//Reset the ROI
		cvResetImageROI(src);
		//System.out.println("r.x()="+r.x()+" ,r.y()="+r.y()+" ,r.width()="+r.width()+" ,r.height()="+r.height());
		
		//Draw the rectangle on the face
		cvRectangle (src, cvPoint(r.x(), r.y()), cvPoint(r.width() + r.x(), r.height() + r.y()), CvScalar.RED, 2, CV_AA, 0);	
		
		String text = "ID Label="+result;
		cvPutText (src, text, cvPoint(r.x(), r.y()), font, CvScalar.GREEN);
		
		canvas.showImage(src);
		}

		//cvWaitKey(0);
		return src;
	}	
	
	private static class SetBoolean extends WindowAdapter
	{
		public void windowClosing(WindowEvent e)
		{
		    isRunning = false;
		}
	}
	
	public static int getPositive()
		{
		    return positive;
		}
	
	public static int getFileNos()
		{
		    return fileNos;
		}
	
}
