package com.sec.frecog;

import static com.googlecode.javacv.cpp.opencv_contrib.*;
import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2GRAY;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;


import static com.googlecode.javacv.cpp.opencv_imgproc.CV_INTER_AREA;

import static com.googlecode.javacv.cpp.opencv_imgproc.cvEqualizeHist;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvResize;

import java.io.File;
import java.io.FilenameFilter;

import com.googlecode.javacv.cpp.opencv_contrib.FaceRecognizer;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_core.MatVector;

public class OpenCVFaceRecognizer {

	 private static final Double THRESHHOLD = 50d;
	//private static final String TRAINING_DIR = "/home/mridup/Desktop/javacv-exp/frecog/training/att_face_min_jpg";
	private static final String TRAINING_DIR = "/home/mridup/Desktop/javacv-exp/frecog/training/Test_Indian_Database/test_Database";
	private static IplImage testImage;
	private static IplImage greyTestImage;
	private static FaceRecognizer faceRecognizer;
	
	public OpenCVFaceRecognizer() {
		//init FaceRecognizer by training the algo with the database
		train_algo();
	}
	
	public static int train_algo() {
				
		File root = new File(TRAINING_DIR);
		
		FilenameFilter pngFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				// return name.toLowerCase().endsWith(".png");
				return name.toLowerCase().endsWith(".jpg");
			}
		};

		File[] imageFiles = root.listFiles(pngFilter);

		MatVector images = new MatVector(imageFiles.length);

		int[] labels = new int[imageFiles.length];

		int counter = 0;
		int label;

		IplImage img;
		IplImage grayImg;

		for (File image : imageFiles) {
			img = cvLoadImage(image.getAbsolutePath());

			label = Integer.parseInt(image.getName().split("\\-")[0]);

			grayImg = IplImage.create(img.width(), img.height(), IPL_DEPTH_8U, 1);

			cvCvtColor(img, grayImg, CV_BGR2GRAY);
			cvEqualizeHist(grayImg, grayImg);
			images.put(counter, grayImg);

			labels[counter] = label;

			counter++;
		}		

		//faceRecognizer = createFisherFaceRecognizer(6, THRESHHOLD );
		faceRecognizer = createEigenFaceRecognizer(0, 5200.0);
		//faceRecognizer = createLBPHFaceRecognizer(1, 8, 8, 8, THRESHHOLD);

		System.out.println("Training "+ counter + " in progress....");
		faceRecognizer.train(images, labels);
		
		return 1;
	}
	
	public static int run(String testImageLocation) {

		testImage = cvLoadImage(testImageLocation);

		greyTestImage = IplImage.create(testImage.width(),
				testImage.height(), IPL_DEPTH_8U, 1);

		cvCvtColor(testImage, greyTestImage, CV_BGR2GRAY);
		cvEqualizeHist(greyTestImage, greyTestImage);
		final int[] prediction = new int[1];
		final double[] confidence = new double[1];
      
		faceRecognizer.predict(greyTestImage, prediction, confidence);

		System.out.println("Predicted label: " + prediction[0]+" confidence "+confidence[0]);
		return prediction[0];
	}
}
