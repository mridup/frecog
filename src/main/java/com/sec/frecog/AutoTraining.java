package com.sec.frecog;

import static com.googlecode.javacv.cpp.opencv_contrib.*;
import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2GRAY;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;


import static com.googlecode.javacv.cpp.opencv_imgproc.CV_INTER_AREA;

import static com.googlecode.javacv.cpp.opencv_imgproc.cvEqualizeHist;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvResize;

import java.io.File;
import java.io.FilenameFilter;
import org.apache.commons.io.FileUtils;
import java.io.IOException;

import com.googlecode.javacv.cpp.opencv_contrib.FaceRecognizer;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_core.MatVector;

import java.io.*; 
import java.awt.event.*; 
import com.googlecode.javacv.*; 
import com.googlecode.javacv.FrameGrabber.Exception;
import static com.googlecode.javacv.cpp.opencv_core.*; 
import static com.googlecode.javacv.cpp.opencv_highgui.*; 
import java.awt.image.BufferedImage;

import static com.googlecode.javacv.cpp.opencv_objdetect.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import static com.googlecode.javacv.cpp.opencv_calib3d.*;

import static com.googlecode.javacv.cpp.opencv_contrib.createFisherFaceRecognizer;


import com.googlecode.javacv.cpp.opencv_contrib.FaceRecognizer;
public class AutoTraining {

	private static boolean isRunning = true;
	public static final String XML_FILE = "/home/mridup/Desktop/javacv-exp/frecog/src/main/resources/haarcascade_frontalface_default.xml";
	private static final Double THRESHHOLD = 50d;
	private static final String AUTO_TRAINING_DIR = "/home/mridup/Desktop/javacv-exp/frecog/training/Test_Indian_Database/Auto_Training_DB";
	private static final String TRAINING_DIR = "/home/mridup/Desktop/javacv-exp/frecog/training/Test_Indian_Database/test_Database";
	private static final String NEW_IMAGES_DIR = "/home/mridup/Desktop/javacv-exp/frecog/training/Test_Indian_Database/test_Database";
	private static IplImage testImage;
	private static IplImage greyTestImage;
	private static FaceRecognizer faceRecognizer;
	private static double threshold=6000.0;
	private static int total_test_images=0;
	
	public AutoTraining() {
		//Init functions
		
	}
	
	
	public static void tuneAlgorithm(/*double currentThresholdValue, int label*/) throws Exception{
		//Build the new database
		buildDatabase();
				
		//FaceDetection and resizing video images in test dir
		GrabAndResize();
		
		//Get the current threshold from OpenCVFaceRecognizer
		//threshold=currentThresholdValue;
		//label:loop
		//Check threshold value and increase valueby 50
		//Train the algorithm with the new database with certain threshold
		//run facerecognizer for each image in test dir
		//Check results to see if the image is detected
		//If positive exit else go back to label:loop
		int result=0;
		System.out.println("Current Threshold: "+ threshold);
		while(true)
		{
			threshold=threshold+50;
			trainAlgo(threshold);
			
			//run recognizer and check results
			result=runRecog();
			if (result==1) break;
			
		}
		
		System.out.println("Algo is tuned with threshold "+ threshold);
		
	}
	
	
	public static void buildDatabase() {
				
		//Build the new database by copying the new images into the database folder
		//Copy all images in IndianDatabase to Test DB Dir and also copy new images
		
		File source = new File(TRAINING_DIR);
		File desc = new File(AUTO_TRAINING_DIR);
		try {
			FileUtils.copyDirectory(source, desc);
		} catch (IOException e) {
		e.printStackTrace();
		}

		File source_img = new File(NEW_IMAGES_DIR);
		try {
			FileUtils.copyDirectory(source, desc);
		} catch (IOException e) {
		e.printStackTrace();
		}
		
		return;
	}
	
	
	public static void GrabAndResize() throws Exception{
	
		IplImage image;
		FrameGrabber grabber = new FFmpegFrameGrabber("/home/mridup/Desktop/javacv-exp/frecog/training/video/amit.mp4"); 
		
		grabber.start();
		
		int i=0;
		while(isRunning)
		{
		if((image = grabber.grab())==null)
		{
			break;
		}
		else
		{
			//System.out.println("Saving image: "+i+"ph.jpg");
			cvSaveImage("/home/mridup/Desktop/javacv-exp/frecog/training/Test_Indian_Database/Auto_Training_Video/grab/"+i+"ph.jpg",image);
		}
		i++;
		}
		
		for(int n = 0; n < i; n++) {
		IplImage img = cvLoadImage("/home/mridup/Desktop/javacv-exp/frecog/training/Test_Indian_Database/Auto_Training_Video/grab/"+n+"ph.jpg");
		detect(img,n);
		}
	}
	
	public static IplImage detect(IplImage src, int no){		
	
		CvHaarClassifierCascade cascade = new CvHaarClassifierCascade(cvLoad(XML_FILE));
		CvMemStorage storage = CvMemStorage.create();
		CvSeq sign = cvHaarDetectObjects(src, cascade, storage, 1.5, 3, CV_HAAR_DO_CANNY_PRUNING);
		CvFont font = new CvFont(CV_FONT_HERSHEY_PLAIN, 2.1, 2); 

		cvClearMemStorage(storage);
		 
		int total_Faces = sign.total();	
		
		//System.out.println("Found " + total_Faces + " face(s)"); 

		for(int i = 0; i < total_Faces; i++) {
		
		CvRect r = new CvRect(cvGetSeqElem(sign, i));
		cvSetImageROI(src, r);//Set the ROI
		//Create a new image
		IplImage crop = cvCreateImage(cvSize(92,112),IPL_DEPTH_8U, 3 );

		//use cvResize to resize source to a destination image
		cvResize(src, crop, CV_INTER_LINEAR);

		cvSaveImage("/home/mridup/Desktop/javacv-exp/frecog/training/Test_Indian_Database/Auto_Training_Video/grab_resize/"+no+"_"+i+"_ncr.jpg", crop);
		total_test_images++;
		}

		return src;
	}	
	
	
	public static void trainAlgo(double threshold) { 	
	
		File root = new File(AUTO_TRAINING_DIR);
		
		FilenameFilter pngFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				// return name.toLowerCase().endsWith(".png");
				return name.toLowerCase().endsWith(".jpg");
			}
		};

		File[] imageFiles = root.listFiles(pngFilter);

		MatVector images = new MatVector(imageFiles.length);

		int[] labels = new int[imageFiles.length];

		int counter = 0;
		int label;

		IplImage img;
		IplImage grayImg;

		for (File image : imageFiles) {
			img = cvLoadImage(image.getAbsolutePath());

			label = Integer.parseInt(image.getName().split("\\-")[0]);

			grayImg = IplImage.create(img.width(), img.height(), IPL_DEPTH_8U, 1);

			cvCvtColor(img, grayImg, CV_BGR2GRAY);
			cvEqualizeHist(grayImg, grayImg);
			images.put(counter, grayImg);

			labels[counter] = label;

			counter++;
		}		

		//Init the faceRecognizer with the new parameters
		faceRecognizer = createEigenFaceRecognizer(0, threshold);

		System.out.println("Re-Training "+ counter + " in progress....");
		faceRecognizer.train(images, labels);
		
		return;
	}
	
	
	public static int runRecog() {
		
		File root = new File("/home/mridup/Desktop/javacv-exp/frecog/training/Test_Indian_Database/Auto_Training_Video/grab_resize");
		
		FilenameFilter jpgFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".jpg");
			}
		};

		File[] imageFiles = root.listFiles(jpgFilter);
		int result=0, positive=0;
		for (File image : imageFiles) {
			result = run(image.getAbsolutePath());
			if (result==4) positive++;
		}
		
		if (positive>0) return 1;
		return 0;
		}
		
		
	public static int run(String testImageLocation) {

		testImage = cvLoadImage(testImageLocation);

		greyTestImage = IplImage.create(testImage.width(),
				testImage.height(), IPL_DEPTH_8U, 1);

		cvCvtColor(testImage, greyTestImage, CV_BGR2GRAY);
		cvEqualizeHist(greyTestImage, greyTestImage);
		final int[] prediction = new int[1];
		final double[] confidence = new double[1];
      
		faceRecognizer.predict(greyTestImage, prediction, confidence);

		System.out.println("Predicted label: " + prediction[0]+" confidence "+confidence[0]);
		return prediction[0];
	}
	

	
	
	public static int updateDB() {
		
		return 0;
		
	}
}

