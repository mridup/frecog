package com.sec.frecog;

import java.io.*; 
import java.awt.event.*; 
import com.googlecode.javacv.*;

import com.googlecode.javacv.cpp.opencv_core.IplImage;
import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_highgui.*;
import static com.googlecode.javacv.cpp.opencv_objdetect.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import static com.googlecode.javacv.cpp.opencv_calib3d.*;
 
public class AddToDatabase{
 	
	public static final String DB_DIR = "/home/mridup/Desktop/javacv-exp/frecog/training/mridu_images/latest3";
	//private static final String DB_RESIZE_DIR = "/home/mridup/Desktop/javacv-exp/frecog/training/Test_Indian_Database/test_Images/12";
	private static final String DB_RESIZE_DIR = "/home/mridup/Desktop/javacv-exp/frecog/training/mridu_images/latest3/resized";
	public static final String XML_FILE = "/home/mridup/Desktop/javacv-exp/frecog/src/main/resources/haarcascade_frontalface_default.xml";
	
	public static void main(String[] args){
		 
		File root = new File(DB_DIR);
		
		FilenameFilter jpgFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".jpg");
			}
		};

		
		File[] imageFiles = root.listFiles(jpgFilter);
		
		System.out.println("Size: "+imageFiles.length);
		
		for (File testImage : imageFiles) {
			System.out.println("Adding File:"+testImage.getAbsolutePath());
			IplImage img = cvLoadImage(testImage.getAbsolutePath());
			detect(testImage, img);
			
		}		
		
	}	
	 
	public static void detect(File file, IplImage src){
		 
		File srcImage = file;
		//String src = file.getAbsolutePath();
		CvHaarClassifierCascade cascade = new CvHaarClassifierCascade(cvLoad(XML_FILE));
		CvMemStorage storage = CvMemStorage.create();
		CvSeq sign = cvHaarDetectObjects(src, cascade, storage, 1.5, 3, CV_HAAR_DO_CANNY_PRUNING);
		 
		cvClearMemStorage(storage);
		 
		int total_Faces = sign.total();	
		
		System.out.println("Found " + total_Faces + " face(s)"); 

		for(int i = 0; i < total_Faces; i++) {
		
		CvRect r = new CvRect(cvGetSeqElem(sign, i));
		cvSetImageROI(src, r);//Set the ROI
		//Create a new image
		IplImage crop = cvCreateImage(cvSize(92,112),IPL_DEPTH_8U, 3 );
		
		//copy the new image
		//cvCopy(src,crop);
		

		//use cvResize to resize source to a destination image
		cvResize(src, crop);

		cvSaveImage(DB_RESIZE_DIR+"/"+file.getName(), crop);
		//Reset the ROI
		cvResetImageROI(src);
		System.out.println("r.x()="+r.x()+" ,r.y()="+r.y()+" ,r.width()="+r.width()+" ,r.height()="+r.height());
		cvRectangle (src, cvPoint(r.x(), r.y()), cvPoint(r.width() + r.x(), r.height() + r.y()),
				CvScalar.RED, 2, CV_AA, 0);		 
		}
	 
		cvShowImage("Result", src);
		//cvWaitKey(0);
	 
	}	
}
