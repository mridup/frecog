package com.sec.frecog;

import static com.googlecode.javacv.cpp.opencv_contrib.createEigenFaceRecognizer;
import static com.googlecode.javacv.cpp.opencv_core.IPL_DEPTH_8U;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2GRAY;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;

import java.io.File;
import java.io.FilenameFilter;

import com.googlecode.javacv.CanvasFrame;
import com.googlecode.javacv.cpp.opencv_contrib.FaceRecognizer;
import com.googlecode.javacv.cpp.opencv_core.IplImage;
import com.googlecode.javacv.cpp.opencv_core.MatVector;

/**
 * Face Recognition
 *
 */
public class App 
{
    public static void main(String[] args) {
        
        // read an image
        final IplImage image = cvLoadImage("/home/mridup/Desktop/javacv-exp/frecog/src/main/resources/lena.png");
        System.out.println("Image loaded: " + image); 
        // create image window named "My Image"
        final CanvasFrame canvas = new CanvasFrame("Mpd Image");
        
        // request closing of the application when the image window is closed
        //canvas.setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);
        canvas.setDefaultCloseOperation(CanvasFrame.EXIT_ON_CLOSE);
        
        // show image on window
        canvas.showImage(image);

	try {
		canvas.waitKey();
		// wait for keypress on canvas
	}
	catch(InterruptedException e) {}

	canvas.dispose();
    }

}
