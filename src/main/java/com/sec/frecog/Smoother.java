package com.sec.frecog;

import static com.googlecode.javacv.cpp.opencv_core.cvReleaseImage;
import static com.googlecode.javacv.cpp.opencv_highgui.cvLoadImage;
import static com.googlecode.javacv.cpp.opencv_highgui.cvSaveImage;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_GAUSSIAN;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvSmooth;

import com.googlecode.javacv.cpp.opencv_core.IplImage;

public class Smoother {
	public void smooth(String filename, String destfilename) {
		IplImage image = cvLoadImage(filename);
		if (image != null) {
			cvSmooth(image, image, CV_GAUSSIAN, 3);
			cvSaveImage(destfilename, image);
			cvReleaseImage(image);
		}
	}
}
