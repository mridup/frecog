package com.sec.frecog;

import java.io.*; 
import java.awt.event.*; 
import com.googlecode.javacv.*; 
import com.googlecode.javacv.FrameGrabber.Exception;
import static com.googlecode.javacv.cpp.opencv_core.*; 
import static com.googlecode.javacv.cpp.opencv_highgui.*; 
import java.awt.image.BufferedImage;

import static com.googlecode.javacv.cpp.opencv_contrib.createFisherFaceRecognizer;
import static com.googlecode.javacv.cpp.opencv_imgproc.CV_BGR2GRAY;
import static com.googlecode.javacv.cpp.opencv_imgproc.cvCvtColor;

import com.googlecode.javacv.cpp.opencv_contrib.FaceRecognizer;

import com.sec.frecog.OpenCVFaceRecognizer;
import com.sec.frecog.FaceDetection;

public class RecordActivity {

	private static boolean isRunning = true;
	private static final String GRAB_RESIZE_DIR = "/home/mridup/Desktop/javacv-exp/frecog/training/video/grab_resize";
	//private static final String GRAB_RESIZE_DIR = "/home/mridup/Desktop/javacv-exp/frecog/training/Test_Indian_Database/test_Images/21";
	//private static final String GRAB_RESIZE_DIR = "/home/mridup/Desktop/javacv-exp/frecog/training/test";
	
	public static void main(String[] args) throws Exception{
		FrameRecorder recorder=null;
		CanvasFrame canvas = new CanvasFrame("Camera");       
		canvas.setDefaultCloseOperation(CanvasFrame.EXIT_ON_CLOSE);
		canvas.addWindowListener(new SetBoolean());

		IplImage image;
		
		OpenCVFaceRecognizer faceRecognizer = new OpenCVFaceRecognizer();
		FaceDetection faceDetector = new FaceDetection(faceRecognizer);
		
		//FrameGrabber grabber = new FFmpegFrameGrabber("/home/mridup/Desktop/javacv-exp/frecog/training/video/guvcview_video-2.mkv"); 
		FrameGrabber grabber = new FFmpegFrameGrabber("/home/mridup/Desktop/javacv-exp/frecog/training/video/my_video_2.avi"); 
		grabber.start();

		/*try {
		recorder = FrameRecorder.createDefault("/home/mridup/Desktop/javacv-exp/frecog/training/video/out/out_video.avi", 240, 360);
		recorder.start();
		}
		catch(FrameRecorder.Exception fE) {
		}
		recorder.setVideoCodec(CV_FOURCC('M','J','P','G'));
		recorder.setVideoBitrate(16);
		recorder.setFrameRate(25);
		recorder.setFormat("avi"); 

		recorder.setPixelFormat(1);*/

		int i=0;
/**/
		while(isRunning)
		{
		if((image = grabber.grab())==null)
		{
			break;
		}
		else
		{
			System.out.println("Saving image: "+i+"ph.jpg");
			cvSaveImage("/home/mridup/Desktop/javacv-exp/frecog/training/video/grab/"+i+"ph.jpg",image);
/**/			/*try {
			recorder.record(image);
			}
			catch(FrameRecorder.Exception fE) {
			}*/ 
			//canvas.showImage(image); 
/**/		}
		i++;
		}
/**/		/*try {
		System.out.println("Stopping recording video.");
		recorder.stop();
		}
		catch(FrameRecorder.Exception fE) {
		}*/
		
		
		//The frames has been stored and now needs to be ROI'd and resized
		//For each frame stored above, iterate through each and detect face and resize it in folder grab_resize
		for(int n = 0; n < i; n++) {
		IplImage img = cvLoadImage("/home/mridup/Desktop/javacv-exp/frecog/training/video/grab/"+n+"ph.jpg");
		faceDetector.detect(img,n);
		}

		//Test FaceRecognizer with the images from video grab
	
		File root = new File(GRAB_RESIZE_DIR);
		
		FilenameFilter jpgFilter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".jpg");
			}
		};

		
		
		/*File[] imageFiles = root.listFiles(jpgFilter);
		int result, fileNos=0, positive=0;
		for (File testImage : imageFiles) {
			System.out.println("Matching File:"+testImage.getAbsolutePath());
			result = faceRecognizer.run(testImage.getAbsolutePath());
			if (result==2) positive++;
			fileNos++;
		}		*/
		
		double ans = (((double)faceDetector.getPositive()/(double)faceDetector.getFileNos())*(double)100);
		System.out.println("Result success rate: "+ ans+"% "+ faceDetector.getPositive()+" out of "+faceDetector.getFileNos());

		}

	private static class SetBoolean extends WindowAdapter
	{
		public void windowClosing(WindowEvent e)
		{
		    isRunning = false;
		}
	}

}
